//
//  EWVCollectionObject.m
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/4/27.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import "EWVCollectionObject.h"
#import <Quartz/Quartz.h>

@implementation EWVCollectionObject

- (NSString *)displayName
{
    NSString *path = [self.fileURL path];
    return [path lastPathComponent];
}

- (NSString *)imageUID
{
    return [self.fileURL path];
}

- (NSString *)imageRepresentationType
{
    return IKImageBrowserNSImageRepresentationType;
}

- (id)imageRepresentation
{
    return self.icon;
}

- (NSString *)imageTitle
{
    return [self.fileURL lastPathComponent];
}

@end
