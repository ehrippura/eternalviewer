//
//  EWVCategory.h
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/1/26.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EWVNode.h"

@class EWVCategoryItem;

@interface EWVCategory : EWVNode <NSSecureCoding>

@property (nonatomic, assign) BOOL expanding;

- (NSArray *)items;
- (void)addItem:(EWVCategoryItem *)item;
- (void)insertItem:(EWVCategoryItem *)item atIndex:(NSInteger)index;
- (void)insertItems:(NSArray *)items atIndex:(NSInteger)index;

- (void)removeItemAtIndex:(NSInteger)index;
- (void)removeItemsInArray:(NSArray *)items;

@end
