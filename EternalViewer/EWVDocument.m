//
//  EWVDocument.m
//  EternalViewer
//
//  Created by 林 子頤 on 13/2/19.
//  Copyright (c) 2013 EternalWind. All rights reserved.
//

#import "EWVDocument.h"
#import "EWVViewerWindow.h"
#import "EWVCategoryItem.h"
#import "EWVPluginManager.h"

#import <EWCompress/EWCompress.h>

@interface EWVDocument () <EWVViewerImageSource, NSWindowDelegate, NSTableViewDataSource, NSTableViewDelegate> {
    NSCache *_imageCache;
}

@property (nonatomic, weak) EWVViewerWindow *viewer;

@end

NSArray *buildFileListWithPath(NSURL *path)
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSDirectoryEnumerator *enumerator =
    [fileManager enumeratorAtURL:path
      includingPropertiesForKeys:@[NSURLNameKey]
                         options:(NSDirectoryEnumerationSkipsSubdirectoryDescendants | NSDirectoryEnumerationSkipsHiddenFiles | NSDirectoryEnumerationSkipsPackageDescendants)
                    errorHandler:nil];
    
    NSURL *url = nil;
    NSMutableArray *containedZipFiles = [[NSMutableArray alloc] init];
    while (url = [enumerator nextObject]) {
        EWCFile *zipFile = [[EWVPluginManager sharedManager] zipFileWithFilename:[url path]];
        if (zipFile) {
            [containedZipFiles addObject:zipFile];
        }
    }
    
    return [containedZipFiles copy];
}

@implementation EWVDocument

- (NSString *)windowNibName
{
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"EWVDocument";
}

- (void)windowControllerDidLoadNib:(NSWindowController *)aController
{
    [super windowControllerDidLoadNib:aController];
    
    EWVViewerWindow *window = (EWVViewerWindow *)[self windowForSheet];
    window.imageSource = self;
    window.delegate = self;
    
    self.viewer = window;
    
    _imageCache = [[NSCache alloc] init];
    [_imageCache setCountLimit:10];
}

- (BOOL)readFromURL:(NSURL *)url ofType:(NSString *)typeName error:(NSError *__autoreleasing *)outError
{
    NSString *path = [url path];
    EWCFile *zipFile = [[EWVPluginManager sharedManager] zipFileWithFilename:path];
    
    if (zipFile) {
        _zipFile = zipFile;
        return YES;
    }
    
    return NO;;
}

// image source
- (NSInteger)numberOfImagesForViewer:(EWVViewerWindow *)viewerWindow
{
    NSInteger count = [[_zipFile compressedFilesList] count];
    [_imageCache setCountLimit:(count / 3 < 10) ? 10 : count / 3];
    return count;
}

- (NSImage *)viewer:(EWVViewerWindow *)viewer needsImageAtIndex:(NSInteger)index
{
    NSArray *array = [_zipFile compressedFilesList];
    EWCItem *item = [array objectAtIndex:index];
    NSString *title = item.title;
    
    NSImage *image = [_imageCache objectForKey:title];
    if (!image) {
        NSData *imageData = [item data];
        image = [[NSImage alloc] initWithData:imageData];
        [_imageCache setObject:image forKey:title];
    }
    
    return image;
}

- (NSString *)viewer:(EWVViewerWindow *)viewer imageTitleAtIndex:(NSInteger)index
{
    NSArray *array = [_zipFile compressedFilesList];
    EWCItem *item = [array objectAtIndex:index];
    
    return item.title;
}

- (BOOL)hasNextImage
{
    NSInteger index = self.viewer.currentIndex;
    return index < [[_zipFile compressedFilesList] count] - 1;
}

- (BOOL)hasPreviousImage
{
    NSInteger index = self.viewer.currentIndex;
    return index != 0;
}

- (BOOL)hasNextFile
{
    if (!self.categoryItem)
        return NO;
    
    NSURL *url = self.categoryItem.pathURL;
    NSArray *contain = buildFileListWithPath(url);
    NSInteger index = [contain indexOfObject:_zipFile];
    
    return index != NSNotFound && index < [contain count] - 1;
}

- (BOOL)hasPreviousFile
{
    if (!self.categoryItem)
        return NO;
    
    NSURL *url = self.categoryItem.pathURL;
    NSArray *contain = buildFileListWithPath(url);
    NSInteger index = [contain indexOfObject:_zipFile];
    
    return index != NSNotFound && index != 0;
}

- (IBAction)toNextItem:(id)sender
{
    EWLog(@"Next");
    NSURL *url = self.categoryItem.pathURL;
    NSArray *contain = buildFileListWithPath(url);
    
    NSInteger index = [contain indexOfObject:_zipFile];
    if (index != NSNotFound && index < [contain count] - 1) {
        // load file
        NSURL *newURL = [[contain objectAtIndex:index + 1] fileURL];
        [[NSDocumentController sharedDocumentController] openDocumentWithContentsOfURL:newURL display:YES completionHandler:^(NSDocument *document, BOOL documentWasAlreadyOpen, NSError *error) {
            __unsafe_unretained EWVDocument *doc = (EWVDocument *)document;
            doc.categoryItem = self.categoryItem;
        }];
        
        [self close];
    }
}

- (IBAction)toPreviousItem:(id)sender
{
    EWLog(@"Previous");
    NSURL *url = self.categoryItem.pathURL;
    NSArray *contain = buildFileListWithPath(url);
    
    NSInteger index = [contain indexOfObject:_zipFile];
    if (index != NSNotFound && index != 0) {
        // load file
        NSURL *newURL = [[contain objectAtIndex:index - 1] fileURL];
        [[NSDocumentController sharedDocumentController] openDocumentWithContentsOfURL:newURL display:YES completionHandler:^(NSDocument *document, BOOL documentWasAlreadyOpen, NSError *error) {
            __unsafe_unretained EWVDocument *doc = (EWVDocument *)document;
            doc.categoryItem = self.categoryItem;
        }];
        
        [self close];
    }
}

#pragma mark - Window Delegate
- (void)windowWillEnterFullScreen:(NSNotification *)notification
{
    EWVViewerWindow *window = [notification object];
    [window setContentBorderThickness:0 forEdge:NSRectEdgeMinY];
    [[window infoField] setHidden:YES];
}

- (void)windowWillExitFullScreen:(NSNotification *)notification
{
    EWVViewerWindow *window = [notification object];
    [window setContentBorderThickness:EWVViewerDefaultContentBorderHeight forEdge:NSRectEdgeMinY];
    [[window infoField] setHidden:NO];
}

#pragma mark - Table View
- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    return [[_zipFile compressedFilesList] count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    NSString *identifier = [tableColumn identifier];
    
    if ([identifier isEqualToString:@"index"]) {
        return @(row + 1);
    } else {
        NSArray *array = [_zipFile compressedFilesList];
        EWCItem *item = [array objectAtIndex:row];
        NSString *title = item.title;
        NSRange extensionRange = [title rangeOfString:@"." options:NSBackwardsSearch];
        return [title substringToIndex:extensionRange.location];
    }
}

@end
