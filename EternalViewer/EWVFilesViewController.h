//
//  EWVFilesViewController.h
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2021/1/10.
//  Copyright © 2021 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

extern NSNotificationName const EWVFilesViewControllerDidPickItemNotification;

@class EWVCollectionObject;

@interface EWVFilesViewController : NSViewController <NSCollectionViewDataSource, NSCollectionViewDelegate>

@property (nonatomic, weak) IBOutlet NSCollectionView *collectionView;

@property (nonatomic, strong) NSArray<EWVCollectionObject *> *zipFiles;

@end

NS_ASSUME_NONNULL_END
