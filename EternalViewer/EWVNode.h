//
//  EWVNode.h
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/04/12.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EWVNode : NSObject <NSSecureCoding, NSPasteboardWriting>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSImage *image;

@end
