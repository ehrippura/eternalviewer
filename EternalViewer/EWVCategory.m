//
//  EWVCategory.m
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/1/26.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import "EWVCategory.h"
#import "EWVCategoryItem.h"

@interface EWVCategoryItem (PrivateMethods)
- (void)__setCategory:(EWVCategory *)category;
@end

@interface EWVCategory () {
    NSMutableArray *_storedItems;
}

@property (nonatomic, strong) NSMutableArray *storedItems;

@end

@implementation EWVCategory

+ (BOOL)supportsSecureCoding {
    return YES;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.name = @"New Category";
        _storedItems = [[NSMutableArray alloc] init];
    }

    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _storedItems = [aDecoder decodeObjectOfClasses:[NSSet setWithObjects:[NSArray class], [EWVCategoryItem class], nil] forKey:@"storedItems"];
        self.expanding = [aDecoder decodeBoolForKey:@"expanding"];
        
        if (!_storedItems) {
            _storedItems = [[NSMutableArray alloc] init];
        } else {
            for (EWVCategoryItem *item in _storedItems) {
                [item __setCategory:self];
            }
        }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:_storedItems forKey:@"storedItems"];
    [aCoder encodeBool:self.expanding forKey:@"expanding"];
}

- (NSArray *)items
{
    return [_storedItems copy];
}

- (void)addItem:(EWVCategoryItem *)item
{
    if ([_storedItems containsObject:item]) {
        [_storedItems removeObject:item];
    }
    
    [item __setCategory:self];
    [_storedItems addObject:item];
}

- (void)insertItem:(EWVCategoryItem *)item atIndex:(NSInteger)index
{
    if ([_storedItems containsObject:item])
        [_storedItems removeObject:item];
    
    [item __setCategory:self];
    [_storedItems insertObject:item atIndex:index];
}

- (void)insertItems:(NSArray *)items atIndex:(NSInteger)index
{
    [items makeObjectsPerformSelector:@selector(__setCategory:) withObject:self];
    [_storedItems insertObjects:items atIndexes:[NSIndexSet indexSetWithIndex:index]];
}

- (void)removeItemAtIndex:(NSInteger)index
{
    [_storedItems removeObjectAtIndex:index];
}

- (void)removeItemsInArray:(NSArray *)items
{
    [_storedItems removeObjectsInArray:items];
}

@end
