//
//  EWVPreference.h
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/1/12.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h>

#define EWPREFOBJ   extern NSString *

EWPREFOBJ EWVFavoriteCategoryKey;
EWPREFOBJ EWVBrowserBackgroundColorUpdateNotification;
EWPREFOBJ EWVBrowserCollectionViewZoomScale;

@interface EWVPreference : NSWindowController <NSToolbarDelegate> {
    IBOutlet NSToolbar *_toolbar;
}

- (IBAction)panelItemSelection:(id)sender;

@end