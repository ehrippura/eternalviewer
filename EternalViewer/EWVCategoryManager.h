//
//  EWVCategoryManager.h
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/04/12.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EWVCategory.h"
#import "EWVCategoryItem.h"

@interface EWVCategoryManager : NSObject {
    NSMutableArray *_categories;
}

+ (instancetype)sharedManager;

- (NSArray *)categories;

- (EWVCategory *)categoryWithName:(NSString *)name;
- (EWVCategory *)addCategoryFromName:(NSString *)name;

- (void)moveCategory:(EWVCategory *)category toIndex:(NSInteger)index;

- (void)save;


@end
