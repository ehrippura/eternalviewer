//
//  EWVContentView.h
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/4/27.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface EWVContentView : NSView

@property (nonatomic, strong) NSImage *image;
@property (nonatomic, assign) BOOL fullscreenMode;

@end
