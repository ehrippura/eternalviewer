//
//  EWVCollectionObject.h
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/4/27.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h>

// avoid use EWCZipFile directly
@interface EWVCollectionObject : NSObject

@property (nonatomic, copy) NSURL *fileURL;
@property (nonatomic, copy) NSImage *icon;

@property (nonatomic, readonly) NSString *displayName;

// IKImageBrowserItem
- (NSString *)imageUID;
- (NSString *)imageRepresentationType;
- (id)imageRepresentation;
- (NSString *)imageTitle;

@end
