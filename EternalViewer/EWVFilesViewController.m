//
//  EWVFilesViewController.m
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2021/1/10.
//  Copyright © 2021 EternalWind. All rights reserved.
//

#import "EWVFilesViewController.h"
#import "EWVFileViewItem.h"

NSNotificationName const EWVFilesViewControllerDidPickItemNotification = @"EWVFilesViewControllerDidPickItemNotification"; 

@interface EWVFilesViewController ()

@end

@implementation EWVFilesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.collectionView registerClass:[EWVFileViewItem class] forItemWithIdentifier:@"CollectionViewItem"];
    self.collectionView.allowsMultipleSelection = YES;
}

- (void)setZipFiles:(NSArray<EWVCollectionObject *> *)zipFiles {
    _zipFiles = zipFiles;
    [self.collectionView reloadData];
}

- (NSCollectionViewItem *)collectionView:(NSCollectionView *)collectionView itemForRepresentedObjectAtIndexPath:(NSIndexPath *)indexPath {
    EWVFileViewItem *item = [collectionView makeItemWithIdentifier:@"CollectionViewItem" forIndexPath:indexPath];
    item.representedObject = self.zipFiles[indexPath.item];
    return item;
}

- (NSInteger)collectionView:(NSCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.zipFiles.count;
}

- (void)collectionView:(NSCollectionView *)collectionView didSelectItemsAtIndexPaths:(NSSet<NSIndexPath *> *)indexPaths {

}

@end
