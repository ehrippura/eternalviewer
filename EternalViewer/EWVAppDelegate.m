//
//  EWVAppDelegate.m
//  EternalViewer
//
//  Created by 林 子頤 on 13/2/19.
//  Copyright (c) 2013 EternalWind. All rights reserved.
//

#import "EWVAppDelegate.h"
#import "EWVMainMenu.h"
#import "EWVCategoryManager.h"

NSURL *applicationFilesLocation()
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *appSupportURL = [[fileManager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    return [appSupportURL URLByAppendingPathComponent:@"EternalViewer"];
}

@implementation EWVAppDelegate

+ (void)initialize
{
    // create application data
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir;
    if (![fileManager fileExistsAtPath:[applicationFilesLocation() path] isDirectory:&isDir]) {
        [fileManager createDirectoryAtURL:applicationFilesLocation() withIntermediateDirectories:YES attributes:nil error:nil];
    } else if (!isDir) {
        [fileManager removeItemAtURL:applicationFilesLocation() error:nil];
        [fileManager createDirectoryAtURL:applicationFilesLocation() withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    // set default preference setting
    NSString *prefPath = [[NSBundle mainBundle] pathForResource:@"defaultSetting" ofType:@"plist"];
    NSDictionary *userDefault = [NSDictionary dictionaryWithContentsOfFile:prefPath];
    [[NSUserDefaults standardUserDefaults] registerDefaults:userDefault];
}

- (void)applicationDidFinishLaunching:(NSNotification *)notification
{
    if ([[[notification userInfo] objectForKey:NSApplicationLaunchIsDefaultLaunchKey] boolValue]) {
        [_mainMenu showBrowser:self];
    }
    
    _documentController = [NSDocumentController sharedDocumentController];
}

- (BOOL)applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag
{
    if (!flag) {
        [_mainMenu showBrowser:self];
    }
    
    return YES;
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
    [[EWVCategoryManager sharedManager] save];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
