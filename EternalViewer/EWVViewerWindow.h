//
//  EWVViewerWindow.h
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/4/26.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol EWVViewerImageSource;

extern const NSInteger EWVViewerDefaultContentBorderHeight;

@interface EWVViewerWindow : NSWindow {
    IBOutlet NSView *_controlView;
    IBOutlet NSTextField *_infoField;
    NSTimer *_dismissControlViewTimer;
}

@property (nonatomic, weak) id <EWVViewerImageSource> imageSource;
@property (nonatomic, readonly) NSUInteger currentIndex;

- (IBAction)nextImageButtonDown:(id)sender;
- (IBAction)previousImageButtonDown:(id)sender;

- (NSTextField *)infoField;

// jump directly by index
- (void)showImageAtIndex:(NSUInteger)index;
- (void)showImageAtIndex:(NSUInteger)index adjustWindowSize:(BOOL)adjust;

@end


@protocol EWVViewerImageSource <NSObject>
@required
- (NSInteger)numberOfImagesForViewer:(EWVViewerWindow *)viewerWindow;
- (NSImage *)viewer:(EWVViewerWindow *)viewer needsImageAtIndex:(NSInteger)index;

@optional
- (NSString *)viewer:(EWVViewerWindow *)viewer imageTitleAtIndex:(NSInteger)index;

@end
