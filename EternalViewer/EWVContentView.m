//
//  EWVContentView.m
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/4/27.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import "EWVContentView.h"
#import "EWVViewerWindow.h"

@implementation EWVContentView

- (void)awakeFromNib
{
    self.fullscreenMode = NO;
}

- (NSTrackingArea *)createTrackingArea
{
    CGRect rect = [self bounds];
    if (!self.fullscreenMode) {
        rect.origin.y += EWVViewerDefaultContentBorderHeight;
        rect.size.height -= EWVViewerDefaultContentBorderHeight;
    }
    
    NSTrackingArea *area = [[NSTrackingArea alloc] initWithRect:rect options:(NSTrackingMouseEnteredAndExited | NSTrackingMouseMoved | NSTrackingActiveInKeyWindow | NSTrackingInVisibleRect) owner:self userInfo:nil];
    
    return area;
}

- (void)updateTrackingAreas
{
    NSArray *trackingArea = [[self trackingAreas] copy];
    for (NSTrackingArea *area in trackingArea) {
        [self removeTrackingArea:area];
    }
    
    [self addTrackingArea:[self createTrackingArea]];
}

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
    
    CGRect rect = [self bounds];
    
    if (!self.fullscreenMode) {
        rect.origin.y = EWVViewerDefaultContentBorderHeight;
        rect.size.height -= EWVViewerDefaultContentBorderHeight;
    }
    
    CGContextRef context = [[NSGraphicsContext currentContext] CGContext];
    CGContextSetRGBFillColor(context, 0, 0, 0, 1);
    CGContextFillRect(context, rect);
    
    if (self.image) {
        CGSize imageSize = self.image.size;
        CGSize rectSize = rect.size;
        
        CGFloat widthFactor = rectSize.width / imageSize.width;
        CGFloat heightFactor = rectSize.height / imageSize.height;
        
        if (widthFactor > heightFactor) {
            rect.size.width = imageSize.width * heightFactor;
            rect.origin.x = (rectSize.width - rect.size.width) / 2.0;
        }
        else {
            rect.size.height = imageSize.height * widthFactor;
            rect.origin.y = (rectSize.height - rect.size.height) / 2.0 + ((self.fullscreenMode) ? 0 : EWVViewerDefaultContentBorderHeight);
        }
        
        rect = CGRectIntegral(rect);
        [self.image drawInRect:rect];
    }
}

@end
