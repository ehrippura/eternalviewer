//
//  EWVMainMenu.h
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2013/12/23.
//  Copyright (c) 2013 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EWVMainMenu : NSObject

@property (nonatomic, strong) IBOutlet id browserController;
@property (nonatomic, weak) IBOutlet id preferenceController;

- (IBAction)showBrowser:(id)sender;
- (IBAction)showPreference:(id)sender;

@end
