//
//  EWVNode.m
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/04/12.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import "EWVNode.h"

@implementation EWVNode

+ (BOOL)supportsSecureCoding {
    return YES;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.name = [aDecoder decodeObjectOfClass:[NSString class] forKey:@"name"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name forKey:@"name"];
}

#pragma mark - Paste Board Support

- (NSArray *)writableTypesForPasteboard:(NSPasteboard *)pasteboard
{
    NSMutableArray *type = [[NSMutableArray alloc] init];
    
    [type addObjectsFromArray:
     [self.name writableTypesForPasteboard:pasteboard]];
    
    [type addObjectsFromArray:
     [self.image writableTypesForPasteboard:pasteboard]];
    
    return type;
}

- (id)pasteboardPropertyListForType:(NSString *)type
{
    if ([type isEqualToString:NSPasteboardTypeString]) {
        return self.name;
    }

    return self.image;
}

@end
