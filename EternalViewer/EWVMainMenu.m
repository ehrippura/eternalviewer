//
//  EWVMainMenu.m
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2013/12/23.
//  Copyright (c) 2013 EternalWind. All rights reserved.
//

#import "EWVMainMenu.h"
#import "EWVBrowserController.h"

@implementation EWVMainMenu

- (void)awakeFromNib {
    [super awakeFromNib];
    self.browserController = [[NSStoryboard storyboardWithName:@"EWVBrowserController" bundle:nil] instantiateInitialController];
}

- (IBAction)showBrowser:(id)sender {
    [self.browserController showWindow:sender];
}

- (IBAction)showPreference:(id)sender {
    [self.preferenceController showWindow:sender];
}

@end
