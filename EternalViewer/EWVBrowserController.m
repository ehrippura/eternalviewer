//
//  EWVBrowserController.m
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2013/12/23.
//  Copyright (c) 2013 EternalWind. All rights reserved.
//

#import "EWVBrowserController.h"
#import "EWVCategoryManager.h"
#import "EWVPreference.h"
#import "EWVCollectionObject.h"
#import "EWVDocument.h"
#import "EWVPluginManager.h"
#import "EWVFilesViewController.h"

#import <EWCompress/EWCompress.h>

#define SPLITE_LEFT_MIN_WIDTH 170.f
#define LOCAL_REORDER_PBOARD_TYPE   @"tw.eternalwind.dragging.reorder"

@interface EWVBrowserController() {
    __weak id _selectedFavorite;
    __weak id _selectedZipFile;
    // drag and drop supports
    NSArray *_draggingNodes;
}

@end

@implementation EWVBrowserController

- (void)windowDidLoad {

    [super windowDidLoad];
    
    // add observer for outline view selection change
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectFavoriteItem:) name:NSOutlineViewSelectionDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(filesBrowserOpenFilesNotification:) name:EWVFilesViewControllerDidPickItemNotification object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Actions

- (IBAction)createNewCategory:(id)sender {

    EWLog(@"Create New Category");
    EWVCategoryManager *manager = [EWVCategoryManager sharedManager];
    [manager addCategoryFromName:nil];
    [_favoriteTableView beginUpdates];
    
    [_favoriteTableView insertItemsAtIndexes:[NSIndexSet indexSetWithIndex:([[manager categories] count] - 1)]
                                    inParent:nil
                               withAnimation:NSTableViewAnimationSlideDown];
    
    [_favoriteTableView endUpdates];
}

- (IBAction)openFolder:(id)sender {

    EWLog(@"Open Folder");

    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    [openPanel setCanChooseDirectories:YES];
    [openPanel setCanChooseFiles:NO];
    [openPanel setResolvesAliases:YES];
    [openPanel setAllowsMultipleSelection:YES];

    [openPanel beginSheetModalForWindow:self.window completionHandler:^(NSModalResponse result) {
        
        if (result != NSModalResponseOK) {
            return;
        }
        
        NSArray *folders = [openPanel URLs];
        __weak EWVCategoryItem *latestItem = nil;
        
        for (NSURL* filename in folders) {
            // add folder to group
            EWVCategoryItem *item = [[EWVCategoryItem alloc] initWithFileURL:filename];
            EWVCategory *defaultCategory = [self->_favoriteTableView itemAtRow:0];
            
            if (![defaultCategory.items containsObject:item]) {
                [defaultCategory addItem:item];
                latestItem = item;
                
                // update table
                NSIndexSet *targetIndex = [NSIndexSet indexSetWithIndex:([[defaultCategory items] count] - 1)];
                if ([self->_favoriteTableView isItemExpanded:defaultCategory]) {
                    [self->_favoriteTableView beginUpdates];
                    [self->_favoriteTableView insertItemsAtIndexes:targetIndex
                                                    inParent:defaultCategory
                                               withAnimation:NSTableViewAnimationSlideDown];
                    [self->_favoriteTableView endUpdates];
                } else {
                    [self->_favoriteTableView reloadItem:defaultCategory reloadChildren:YES];
                    [self->_favoriteTableView expandItem:defaultCategory];
                }
            }
        }
        
        if (latestItem) {
            NSInteger row = [self->_favoriteTableView rowForItem:latestItem];
            [self->_favoriteTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:row] byExtendingSelection:NO];
            [self selectFavoriteItem:self->_favoriteTableView];
        }
    }]; // end of open panel
}

- (IBAction)removeFolder:(id)sender {

    // indicate location of selected row
    NSInteger selectedIndex = [_favoriteTableView selectedRow];
    
    // if method sent from contexual menu
    // should change selected item index to mouse clicked location
    if ([sender isKindOfClass:[NSMenuItem class]]) {
        selectedIndex = [_favoriteTableView clickedRow];
    }
    
    // get item from target row
    EWVCategoryItem *item = [_favoriteTableView itemAtRow:selectedIndex];
    
    // get item category
    EWVCategory *category = [_favoriteTableView parentForItem:item];
    // get index of item in category
    NSInteger itemCategoryIndex = [category.items indexOfObject:item];
    
    [_favoriteTableView beginUpdates];
    [category removeItemAtIndex:itemCategoryIndex];
    [_favoriteTableView removeItemsAtIndexes:[NSIndexSet indexSetWithIndex:itemCategoryIndex] inParent:category withAnimation:NSTableViewAnimationSlideUp];
    [_favoriteTableView endUpdates];
}

#pragma mark - Messages
// return if row can be removed
- (BOOL)canRowRemoved {
    NSInteger selectedIndex = [_favoriteTableView selectedRow];
    return (selectedIndex != -1);
}

- (IBAction)selectFavoriteItem:(id)sender {

    NSOutlineView *_favoriteTableView = [sender object];

    NSInteger row = [_favoriteTableView selectedRow];
    EWVCategoryItem *item = [_favoriteTableView itemAtRow:row];
    
    if (item == _selectedFavorite)
        return;
    
    NSURL *path = item.pathURL;
    if (!path) return;

    _selectedFavorite = item;
    
    // load zip file
    NSFileManager *fileManager = [NSFileManager defaultManager];

    NSError *error = nil;
    NSArray<NSURL *> *urls = [fileManager contentsOfDirectoryAtURL:path
               includingPropertiesForKeys:@[NSURLNameKey]
                                  options:(NSDirectoryEnumerationSkipsSubdirectoryDescendants | NSDirectoryEnumerationSkipsHiddenFiles | NSDirectoryEnumerationSkipsPackageDescendants)
                                    error:&error];

    if (error) {
        NSLog(@"%@", error.localizedDescription);
        return;
    }

    self.containedZipFiles = [[NSMutableArray alloc] init];
    for (NSURL *url in urls) {
        EWCFile *zipFile = [[EWVPluginManager sharedManager] zipFileWithFilename:[url path]];
        if (zipFile) {
            EWVCollectionObject *object = [EWVCollectionObject new];
            object.fileURL = url;
            object.icon = zipFile.icon;
            [self.containedZipFiles addObject:object];
        }
    }
    
    if ([self.containedZipFiles count] != 0) {
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES];
        [self.containedZipFiles sortUsingDescriptors:@[sort]];
    }

    // set files
    NSSplitViewController *splitController = (NSSplitViewController *)self.contentViewController;
    NSSplitViewItem *collectionItem = splitController.splitViewItems[1];
    EWVFilesViewController *fileController = (EWVFilesViewController *)collectionItem.viewController;
    fileController.zipFiles = self.containedZipFiles;
    
    // set path view
    [_pathControl setURL:item.pathURL];
}

- (IBAction)showInFinder:(id)sender
{
    NSInteger clickedIndex = [_favoriteTableView clickedRow];
    EWVCategoryItem *item = [_favoriteTableView itemAtRow:clickedIndex];
    
    [[NSWorkspace sharedWorkspace] activateFileViewerSelectingURLs:@[item.pathURL]];
}

- (IBAction)showFileInFinder:(id)sender
{
    EWVCollectionObject *object = _selectedZipFile;
    [[NSWorkspace sharedWorkspace] activateFileViewerSelectingURLs:@[object.fileURL]];
}

- (IBAction)openItem:(id)sender
{
//    NSInteger index = [self.containedZipFiles indexOfObject:_selectedZipFile];
//    [self imageBrowser:_folderCollectionView cellWasDoubleClickedAtIndex:index];
}

- (void)filesBrowserOpenFilesNotification:(NSNotification *)notification {

    NSDictionary *userInfo = notification.userInfo;
    EWVCollectionObject *object = userInfo[@"item"];
    if (object && [object isKindOfClass:[EWVCollectionObject class]]) {
        [self openZipFile:object];
    }
}

- (void)openZipFile:(EWVCollectionObject *)zipFileObject {
    if (zipFileObject) {
        [[NSDocumentController sharedDocumentController] openDocumentWithContentsOfURL:zipFileObject.fileURL display:YES completionHandler:^(NSDocument *document, BOOL documentWasAlreadyOpen, NSError *error) {
            if (!error) {
                __unsafe_unretained EWVDocument *doc = (EWVDocument *)document;
                doc.categoryItem = self->_selectedFavorite;
            } else {
                NSLog(@"%@", [error localizedDescription]);
            }
        }];
    }
}

#pragma mark - Collection View Data Source and Delegate

- (NSInteger)collectionView:(NSCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.containedZipFiles count];
}

- (NSCollectionViewItem *)collectionView:(NSCollectionView *)collectionView itemForRepresentedObjectAtIndexPath:(NSIndexPath *)indexPath {

    NSCollectionViewItem *item = [collectionView makeItemWithIdentifier:@"CollectionViewItem" forIndexPath:indexPath];

    EWVCollectionObject *object = self.containedZipFiles[indexPath.item];
    item.representedObject = object;
    item.imageView.image = object.icon;

    return item;
}

- (id)imageBrowser:(IKImageBrowserView *)aBrowser itemAtIndex:(NSUInteger)index
{
    return [self.containedZipFiles objectAtIndex:index];
}

- (void)imageBrowserSelectionDidChange:(IKImageBrowserView *)aBrowser
{
    EWVCollectionObject *file = nil;
    @try {
        file = [self.containedZipFiles objectAtIndex:[[aBrowser selectionIndexes] firstIndex]];
    }
    @catch (NSException *exception) {
        file = nil;
    }

    if (file) {
        [_pathControl setURL:file.fileURL];
    } else {
        NSInteger row = [_favoriteTableView selectedRow];
        EWVCategoryItem *item = [_favoriteTableView itemAtRow:row];
        [_pathControl setURL:item.pathURL];
    }
    
    _selectedZipFile = file;
}

- (void)imageBrowser:(IKImageBrowserView *) aBrowser cellWasDoubleClickedAtIndex:(NSUInteger) index
{
    EWVCollectionObject *file = _selectedZipFile;
    
    if (file) {
        [[NSDocumentController sharedDocumentController] openDocumentWithContentsOfURL:file.fileURL display:YES completionHandler:^(NSDocument *document, BOOL documentWasAlreadyOpen, NSError *error) {
            if (!error) {
                __unsafe_unretained EWVDocument *doc = (EWVDocument *)document;
                doc.categoryItem = self->_selectedFavorite;
            } else {
                NSLog(@"%@", [error localizedDescription]);
            }
        }];
    }
}

- (void)imageBrowser:(IKImageBrowserView *) aBrowser cellWasRightClickedAtIndex:(NSUInteger) index withEvent:(NSEvent *) event
{
    [NSMenu popUpContextMenu:_collectionMenu withEvent:event forView:aBrowser];
}

#pragma mark - User Input Event
- (void)keyDown:(NSEvent *)theEvent {

    NSString *chars = [theEvent characters];
    unichar firstchar = [chars characterAtIndex:0];
    
    if (firstchar == NSDeleteCharacter && [self canRowRemoved]) {
        [self removeFolder:theEvent];
    } else {
        [super keyDown:theEvent];
    }
}

@end
