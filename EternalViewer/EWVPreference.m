//
//  EWVPreference.m
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/1/12.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import "EWVPreference.h"

NSString *EWVFavoriteCategoryKey = @"tw.eternalwind.viewer.favorite.category";
NSString *EWVPreferenceBackgroundColorKey = @"tw.eternalwind.viewer.pref.bgcolor";
NSString *EWVBrowserBackgroundColorUpdateNotification = @"EWVBrowserBackgroundColorUpdateNotification";
NSString *EWVBrowserCollectionViewZoomScale = @"EWVBrowserCollectionViewZoomScale";

@implementation EWVPreference

- (id)init
{
    return [super initWithWindowNibName:@"EWVPreference"];
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    [_toolbar setSelectedItemIdentifier:@"EWVFontAndColor"];
}

- (IBAction)backgroundColorUpdate:(id)sender
{
    CGFloat level = [(NSSlider *)sender floatValue];
    NSColor *color = [NSColor colorWithGenericGamma22White:level alpha:1.0];

    [[NSUserDefaults standardUserDefaults] setFloat:level forKey:EWVPreferenceBackgroundColorKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:EWVBrowserBackgroundColorUpdateNotification object:nil userInfo:@{@"color":color}];
}

- (IBAction)panelItemSelection:(id)sender
{
    
}

- (NSArray *)toolbarSelectableItemIdentifiers:(NSToolbar *)toolbar
{
    return @[@"EWVFontAndColor"];
}

@end
