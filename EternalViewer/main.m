//
//  main.m
//  EternalViewer
//
//  Created by 林 子頤 on 13/2/19.
//  Copyright (c) 2013 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
