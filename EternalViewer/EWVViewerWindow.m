//
//  EWVViewerWindow.m
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/4/26.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import "EWVViewerWindow.h"
#import "EWVContentView.h"

#import <QuartzCore/QuartzCore.h>

const NSInteger EWVViewerDefaultContentBorderHeight = 25;

@interface EWVViewerWindow () {
    NSUInteger _numOfImage;
    NSUInteger _currentIndex;
}

- (void)resetTimer;
- (void)fireHiddenTimer:(NSTimer *)timer;

- (void)adjustWindowFrameForImage:(NSImage *)image;
- (BOOL)isFullScreen;

@property (nonatomic, weak) EWVContentView *aContent;

@end

@implementation EWVViewerWindow

- (void)awakeFromNib
{
    // set content view
    self.aContent = [self contentView];
    
    // initialize control view
    CALayer *layer = _controlView.layer;

    // set control view background color
    CGColorRef color = [NSColor colorWithCalibratedWhite:.2f alpha:.8f].CGColor;
    
    if (color) {
        [layer setBackgroundColor:color];
    }
    
    layer.cornerRadius = 5.f;
    layer.opacity = 0.f;

    [self resetTimer];
}

- (void)toggleFullScreen:(id)sender
{
    [(EWVContentView*)[self contentView] setFullscreenMode:(sender != nil)];
    [super toggleFullScreen:sender];
}

// setter / getter
- (void)setImageSource:(id<EWVViewerImageSource>)imageSource
{
    if (_imageSource != imageSource) {
        [self willChangeValueForKey:@"imageSource"];
        _imageSource = imageSource;
        [self didChangeValueForKey:@"imageSource"];
        
        _numOfImage = [_imageSource numberOfImagesForViewer:self];
        
        // load first image
        if (_numOfImage != 0) {
            [self showImageAtIndex:0 adjustWindowSize:YES];
        }
    }
}

- (BOOL)isFullScreen
{
    NSUInteger styleMask = [self styleMask];
    return (styleMask & NSWindowStyleMaskFullScreen) != 0;
}

- (void)adjustWindowFrameForImage:(NSImage *)image
{
    NSSize screenSize = [[self screen] visibleFrame].size;
    NSSize imageSize = image.size;
    NSSize targetSize = NSZeroSize;
    NSSize contentMinSize = [self contentMinSize];

    if (imageSize.width > imageSize.height) {
        CGFloat width = screenSize.width * 0.9;
        CGFloat ratio = width / imageSize.width;
        CGFloat height = imageSize.height * ratio;
        
        targetSize = NSMakeSize(width, height);
    } else {
        CGFloat height = screenSize.height * 0.9;
        CGFloat ratio = height / imageSize.height;
        CGFloat width = imageSize.width * ratio;
        targetSize = NSMakeSize(width, height);
    }
    
    if (![self isFullScreen])
        targetSize.height += EWVViewerDefaultContentBorderHeight;
    
    if (targetSize.height < contentMinSize.height)
        targetSize.height = contentMinSize.height;
    
    if (targetSize.width < contentMinSize.width)
        targetSize.width = contentMinSize.width;
    
    [self setContentSize:targetSize];
    [self center];
}

- (void)showImageAtIndex:(NSUInteger)index adjustWindowSize:(BOOL)adjust
{
    _currentIndex = index;
    NSImage *image = [_imageSource viewer:self needsImageAtIndex:index];
    if (adjust)
        [self adjustWindowFrameForImage:image];
    
    self.aContent.image = image;
    [self.aContent setNeedsDisplay:YES];
    
    if ([_imageSource respondsToSelector:@selector(viewer:imageTitleAtIndex:)]) {
        NSString *title = [_imageSource viewer:self imageTitleAtIndex:index];
        [_infoField setStringValue:title];
    }
}

- (void)showImageAtIndex:(NSUInteger)index
{
    [self showImageAtIndex:index adjustWindowSize:NO];
}

- (NSTextField *)infoField
{
    return _infoField;
}

// event
- (IBAction)nextImageButtonDown:(id)sender
{
    if (_currentIndex < _numOfImage - 1) {
        [self showImageAtIndex:_currentIndex + 1];
        if ([sender isKindOfClass:[NSButton class]])
            [self resetTimer];
    }
}

- (IBAction)previousImageButtonDown:(id)sender
{
    if (_currentIndex != 0) {
        [self showImageAtIndex:_currentIndex - 1];
        
        if ([sender isKindOfClass:[NSButton class]])
            [self resetTimer];
    }
}

- (void)resetTimer
{
    if (_dismissControlViewTimer)
        [_dismissControlViewTimer invalidate];
    
    _dismissControlViewTimer = [NSTimer scheduledTimerWithTimeInterval:2.f target:self selector:@selector(fireHiddenTimer:) userInfo:nil repeats:NO];
}

- (void)fireHiddenTimer:(NSTimer *)timer
{
    // hide control view
    [CATransaction begin];
    [CATransaction setAnimationDuration:.8f];
    [[_controlView layer] setOpacity:0.f];
    [CATransaction commit];
    
    // hide mouse cursor
    [NSCursor setHiddenUntilMouseMoves:YES];
}

// mouse and keyboard event
- (void)mouseMoved:(NSEvent *)theEvent
{
    [CATransaction begin];
    [CATransaction setAnimationDuration:.8f];
    [[_controlView layer] setOpacity:1.f];
    [CATransaction commit];
    
    [self resetTimer];
}

- (void)mouseEntered:(NSEvent *)theEvent
{
    [CATransaction begin];
    [CATransaction setAnimationDuration:.8f];
    [[_controlView layer] setOpacity:1.f];
    [CATransaction commit];
    
    [self resetTimer];
}

- (void)mouseExited:(NSEvent *)theEvent
{
    [CATransaction begin];
    [CATransaction setAnimationDuration:.8f];
    [[_controlView layer] setOpacity:0.f];
    [CATransaction commit];
    
    [self resetTimer];
}

- (void)keyDown:(NSEvent *)theEvent
{
    NSString *characters = [theEvent characters];
    unichar character = [characters characterAtIndex:0];
    
    switch (character) {
        case NSLeftArrowFunctionKey:
            [self previousImageButtonDown:nil];
            break;
            
        case NSRightArrowFunctionKey:
            [self nextImageButtonDown:nil];
            break;
            
        default:
            break;
    }
}

@end
