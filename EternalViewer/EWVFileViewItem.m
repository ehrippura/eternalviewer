//
//  EWVFileViewItem.m
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2021/12/25.
//  Copyright © 2021 EternalWind. All rights reserved.
//

#import "EWVFileViewItem.h"
#import "EWVCollectionObject.h"
#import "EWVFilesViewController.h"

@interface EWVFileViewItem ()

@end

@implementation EWVFileViewItem

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.layer.cornerRadius = 10.0;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    if (selected) {
        self.view.layer.backgroundColor = [NSColor selectedContentBackgroundColor].CGColor;
        self.textField.textColor = [NSColor highlightColor];
    } else {
        self.view.layer.backgroundColor = nil;
        self.textField.textColor = [NSColor labelColor];
    }
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];
    if (representedObject) {
        EWVCollectionObject *obj = representedObject;
        self.imageView.image = obj.icon;
        self.textField.stringValue = obj.displayName;
    }
}

- (void)mouseDown:(NSEvent *)event {
    [super mouseDown:event];
    if (event.clickCount == 2) {
        EWVCollectionObject *obj = self.representedObject;
        [[NSNotificationCenter defaultCenter] postNotificationName:EWVFilesViewControllerDidPickItemNotification object:nil userInfo:@{@"item" : obj}];
    }
}

@end
