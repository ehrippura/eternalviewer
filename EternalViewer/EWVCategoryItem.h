//
//  EWVCategoryItem.h
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/1/27.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EWVNode.h"

@class EWVCategory;

@interface EWVCategoryItem : EWVNode <NSSecureCoding, NSPasteboardReading> {
    NSURL *_pathURL;
}

- (instancetype)initWithPath:(NSString *)path;

- (instancetype)initWithFileURL:(NSURL *)url;

@property (nonatomic, readonly, copy) NSURL *pathURL;
@property (nonatomic, readonly, weak) EWVCategory *category;

@end
