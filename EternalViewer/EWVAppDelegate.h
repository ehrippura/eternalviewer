//
//  EWVAppDelegate.h
//  EternalViewer
//
//  Created by 林 子頤 on 13/2/19.
//  Copyright (c) 2013 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h>

@class EWVMainMenu;

extern NSURL *applicationFilesLocation(void);

@interface EWVAppDelegate : NSObject <NSApplicationDelegate> {
    IBOutlet EWVMainMenu *_mainMenu;
}

@property (nonatomic, readonly) NSDocumentController *documentController;

@end
