//
//  EWVSidebarViewController.m
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2021/1/9.
//  Copyright © 2021 EternalWind. All rights reserved.
//

#import "EWVSidebarViewController.h"
#import "EWVCategoryManager.h"

#define LOCAL_REORDER_PBOARD_TYPE   @"tw.eternalwind.dragging.reorder"

@interface EWVSidebarViewController () {
    IBOutlet NSOutlineView *_favoriteTableView;
    NSArray *_draggingNodes;
}
@end

@implementation EWVSidebarViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // register outline drag and drop
    [_favoriteTableView registerForDraggedTypes:@[ LOCAL_REORDER_PBOARD_TYPE, NSPasteboardTypeFileURL ]];
    [_favoriteTableView setDraggingSourceOperationMask:NSDragOperationEvery forLocal:YES];
    [_favoriteTableView setDraggingSourceOperationMask:NSDragOperationEvery forLocal:NO];
}

- (void)viewWillAppear {
    [super viewWillAppear];

    // expand or collapse items
    for (EWVCategory *category in [[EWVCategoryManager sharedManager] categories]) {
        SEL selector = category.expanding ? @selector(expandItem:) : @selector(collapseItem:);
        [_favoriteTableView performSelector:selector withObject:category afterDelay:0];
    }
}

- (IBAction)openFolder:(id)sender {

    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    [openPanel setCanChooseDirectories:YES];
    [openPanel setCanChooseFiles:NO];
    [openPanel setResolvesAliases:YES];
    [openPanel setAllowsMultipleSelection:YES];

    [openPanel beginSheetModalForWindow:self.view.window completionHandler:^(NSInteger result) {

        if (result != NSModalResponseOK) {
            return;
        }

        NSArray *folders = [openPanel URLs];
        EWVCategoryItem *latestItem = nil;

        for (NSURL* filename in folders) {
            // add folder to group
            EWVCategoryItem *item = [[EWVCategoryItem alloc] initWithFileURL:filename];
            EWVCategory *defaultCategory = [self->_favoriteTableView itemAtRow:0];

            if (![defaultCategory.items containsObject:item]) {
                [defaultCategory addItem:item];
                latestItem = item;

                // update table
                NSIndexSet *targetIndex = [NSIndexSet indexSetWithIndex:([[defaultCategory items] count] - 1)];
                if ([self->_favoriteTableView isItemExpanded:defaultCategory]) {
                    [self->_favoriteTableView beginUpdates];
                    [self->_favoriteTableView insertItemsAtIndexes:targetIndex
                                                          inParent:defaultCategory
                                                     withAnimation:NSTableViewAnimationSlideDown];
                    [self->_favoriteTableView endUpdates];
                } else {
                    [self->_favoriteTableView reloadItem:defaultCategory reloadChildren:YES];
                    [self->_favoriteTableView expandItem:defaultCategory];
                }
            }
        }

        if (latestItem) {
            NSInteger row = [self->_favoriteTableView rowForItem:latestItem];
            [self->_favoriteTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:row] byExtendingSelection:NO];
        }
    }]; // end of open panel
}

- (IBAction)showInFinder:(id)sender {
    NSInteger clickedIndex = [_favoriteTableView clickedRow];
    EWVCategoryItem *item = [_favoriteTableView itemAtRow:clickedIndex];

    [[NSWorkspace sharedWorkspace] activateFileViewerSelectingURLs:@[item.pathURL]];
}

#pragma mark - outline view datasource and delegate

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item {

    if (item == nil) {
        return [[[EWVCategoryManager sharedManager] categories] count];
    }

    EWVCategory *category = item;
    return [[category items] count];
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item {
    return [item isKindOfClass:[EWVCategory class]];
}

- (void)outlineViewItemDidExpand:(NSNotification *)notification {
    EWVCategory *category = [[notification userInfo] objectForKey:@"NSObject"];
    category.expanding = YES;
}

- (void)outlineViewItemDidCollapse:(NSNotification *)notification {
    EWVCategory *category = [[notification userInfo] objectForKey:@"NSObject"];
    category.expanding = NO;
}

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item {
    if (!item) {
        return [[[EWVCategoryManager sharedManager] categories] objectAtIndex:index];
    }
    __unsafe_unretained EWVCategory *category = item;
    return [[category items] objectAtIndex:index];
}

- (NSView *)outlineView:(NSOutlineView *)outlineView viewForTableColumn:(NSTableColumn *)tableColumn item:(id)item {

    NSTableCellView *cell = nil;
    EWVNode *node = item;
    if ([item isKindOfClass:[EWVCategory class]]) {
        cell = [outlineView makeViewWithIdentifier:@"HeaderCell" owner:self];
        [cell.textField setStringValue:[node.name uppercaseString]];
    } else {
        cell = [outlineView makeViewWithIdentifier:@"DataCell" owner:self];
        [cell.textField setStringValue:node.name];
    }

    return cell;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isGroupItem:(id)item {
    return [item isKindOfClass:[EWVCategory class]];
}

// selection
- (BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(id)item {
    return ![item isKindOfClass:[EWVCategory class]];
}

// name edit
- (void)outlineView:(NSOutlineView *)outlineView setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn byItem:(id)item {
    NSAssert([object isKindOfClass:[NSString class]], @"class error");
    EWVNode *node = item;
    node.name = object;
}

#pragma mark - Drag and Drop Supports

- (void)outlineView:(NSOutlineView *)outlineView draggingSession:(NSDraggingSession *)session willBeginAtPoint:(NSPoint)screenPoint forItems:(NSArray *)draggedItems {
    EWLog(@"Begin Drag Session: %@", draggedItems);
    _draggingNodes = draggedItems;
    [session.draggingPasteboard setData:[NSData data] forType:LOCAL_REORDER_PBOARD_TYPE];
}

- (id <NSPasteboardWriting>)outlineView:(NSOutlineView *)outlineView pasteboardWriterForItem:(id)item {
    return item;
}

- (NSDragOperation)outlineView:(NSOutlineView *)outlineView validateDrop:(id<NSDraggingInfo>)info proposedItem:(id)item proposedChildIndex:(NSInteger)index {

    // define by destination
    NSDragOperation operation = NSDragOperationMove;
    __unsafe_unretained EWVNode *target = item;
    __unsafe_unretained EWVNode *source = [_draggingNodes firstObject];

    if (index != NSOutlineViewDropOnItemIndex) {
        if (!source) {
            NSPasteboard *pboard = [info draggingPasteboard];
            NSFileManager *fm = [NSFileManager defaultManager];
            NSData *data = nil;
            NSError *error = nil;
            if ([[pboard types] containsObject:NSPasteboardTypeFileURL]) {

                data = [pboard dataForType:NSPasteboardTypeFileURL];
                NSArray *filenames = [NSPropertyListSerialization propertyListWithData:data
                                                                               options:NSPropertyListImmutable
                                                                                format:nil
                                                                                 error:&error];
                BOOL pass = NO;
                for (NSString *filename in filenames) {
                    BOOL isDict;
                    [fm fileExistsAtPath:filename isDirectory:&isDict];
                    if (isDict) {
                        pass = YES;
                        break;
                    }
                }

                if (!pass)
                    operation = NSDragOperationNone;
            }
        } else if ([source isKindOfClass:[EWVCategory class]]) {
            NSArray *categories = [EWVCategoryManager sharedManager].categories;
            if ([target isKindOfClass:[EWVCategory class]] || [categories indexOfObject:source] == 0)
                operation = NSDragOperationNone;
        } else {
            if (!item)
                operation = NSDragOperationNone;
        }
    } else {
        operation = NSDragOperationNone;
    }

    return operation;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView acceptDrop:(id<NSDraggingInfo>)info item:(id)item childIndex:(NSInteger)index {

    EWVNode *source = [_draggingNodes firstObject];

    if (!source) { // drag from outside of application
        NSArray *classes = @[[EWVCategoryItem class]];
        NSMutableArray *updateItem = [NSMutableArray array];

        [info enumerateDraggingItemsWithOptions:0 forView:_favoriteTableView classes:classes searchOptions:@{} usingBlock:^(NSDraggingItem *draggingItem, NSInteger idx, BOOL *stop) {
            EWVCategoryItem *item = draggingItem.item;
            [updateItem addObject:item];
        }];

        [_favoriteTableView beginUpdates];

        EWVCategory *category = item;
        NSInteger iterator = 0;

        for (EWVCategoryItem *obj in updateItem) {
            if (![category.items containsObject:obj]) {
                [category insertItem:obj atIndex:(index + iterator)];
                [_favoriteTableView insertItemsAtIndexes:[NSIndexSet indexSetWithIndex:(index + iterator++)]
                                                inParent:category
                                           withAnimation:NSTableViewAnimationEffectGap];
            }
        }

        [_favoriteTableView endUpdates];

        return YES;
    } else if ([source isKindOfClass:[EWVCategoryItem class]]) {
        EWVCategory *category = item;
        NSInteger oldIndex = [category.items indexOfObject:source];
        NSInteger newIndex = (oldIndex < index) ? index - 1 : index;

        // reorder
        [_favoriteTableView beginUpdates];

        [_favoriteTableView moveItemAtIndex:oldIndex inParent:[(EWVCategoryItem *)source category] toIndex:newIndex inParent:category];
        [category insertItem:(EWVCategoryItem *)source atIndex:newIndex];

        [_favoriteTableView endUpdates];

        return YES;
    } else {
        NSInteger oldIndex = [[EWVCategoryManager sharedManager].categories indexOfObject:source];

        [_favoriteTableView beginUpdates];

        [_favoriteTableView moveItemAtIndex:oldIndex inParent:nil toIndex:index inParent:nil];
        [[EWVCategoryManager sharedManager] moveCategory:(EWVCategory *)source toIndex:index];

        [_favoriteTableView endUpdates];
    }

    return NO;
}

- (void)outlineView:(NSOutlineView *)outlineView draggingSession:(NSDraggingSession *)session endedAtPoint:(NSPoint)screenPoint operation:(NSDragOperation)operation {
    EWLog(@"End Drag Session");
    _draggingNodes = nil;
}

@end
