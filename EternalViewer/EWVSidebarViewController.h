//
//  EWVSidebarViewController.h
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2021/1/9.
//  Copyright © 2021 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface EWVSidebarViewController : NSViewController <NSOutlineViewDelegate, NSOutlineViewDataSource>

@end

NS_ASSUME_NONNULL_END
