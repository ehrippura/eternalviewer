//
//  EWVPluginManager.h
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/5/10.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EWCompress/EWCompress.h>

@interface EWVPluginManager : NSObject

+ (instancetype)sharedManager;

- (BOOL)fileTypeSupported:(NSString *)type;
- (Class)pricipalClassForFileType:(NSString *)type;

// proxy method
- (EWCFile *)zipFileWithFilename:(NSString *)filename;

@end
