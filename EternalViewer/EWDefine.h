//
//  EWDefine.h
//  Eternal Wind
//
//  Created by Tzu-Yi Lin on 12/8/1.
//  Copyright (c) 2012 Eternal Wind. All rights reserved.
//

#ifndef EeternalWind_Define
#define EeternalWind_Define

#ifdef DEBUG
#define EWLog   NSLog
#else
#define EWLog(x, ...)
#endif

#if defined(__has_feature) && __has_feature(objc_arc)
#define EW_ARC_ENABLE       1
#else
#define EW_ARC_ENABLE       0
#endif

#define EWLocalString(str)                  NSLocalizedString(str, nil)
#define EWLocalStringFromTable(str, table)  NSLocalizedStringFromTable(str, table, nil)

// method be deprecated
#define EW_DEPRECATED     __attribute__((deprecated))

#ifdef __cplusplus
#define EW_EXTERN         extern "C" __attribute__((visibility ("default")))
#else
#define EW_EXTERN         extern __attribute__((visibility ("default")))
#endif

#define EW_MINUTE   60
#define EW_HOUR     (60 * EW_MINUTE)
#define EW_DAY      (24 * EW_HOUR)
#define EW_WEEK     (7 * EW_DAY)
#define EW_MONTH    (30.5 * EW_DAY)
#define EW_YEAR     (365 * EW_DAY)
#define EW_YEAR_M   (366 * EW_DAY)

// safe release objc objects
#if __has_feature(objc_arc)
#define EWRelease(_instance)    { _instance = nil; } ((void)0)
#else
#define EWRelease(_instance)    { [_instance release]; _instance = nil; } ((void)0)
#endif
// safe release for core foundation objects
#define EWCFRelease(_point)     { if (_point) {CFRelease(_point); _point = NULL;} } ((void)0)
// safe invalidate timer
#define EWInvalidateTimer(_timer) { [_timer invalidate]; _timer = nil; } ((void)0)


// active(UI and networking should be updated continuously) delay method
#define EWWaitForSeconds(_seconds) { [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:_seconds]]; } ((void)0)

#define EW_NO_OBJC_CLASS_INSTANCE  \
+ (id)alloc { return nil; } \
- (id)init { return nil; } \
+ (id)allocWithZone:(NSZone *)zone { return nil; } \
- (id)copyWithZone:(NSZone *)zone {return nil;}

#if !__has_feature(objc_arc)
#define EW_CREATE_SINGLETON(className) \
static className *shared ## className = nil; \
\
+ (className *)shared ## className \
{ \
    @synchronized(self) { \
        if (shared ## className) == nil) { \
            shared ## className = [[self alloc] init]; \
        } \
    } \
\
    return shared ## className \
} \
\
+ (id)allocWithZone:(NSZone *)zone \
{ \
    @synchronized(self) { \
        if (shared ## className == nil) { \
            shared ## className = [super allocWithZone:zone]; \
            return shared ## className; \
        } \
    } \
\
    return nil; \
} \
\
- (id)copyWithZone:(NSZone *)zone \
{ \
    return self; \
} \
\
- (id)retain \
{ \
    return self; \
} \
\
- (NSUInteger)retainCount \
{ \
    return NSUIntegerMax; \
} \
\
- (oneway void)release \
{ \
} \
\
- (id)autorelease \
{ \
    return self; \
}
#else
#define EW_CREATE_SINGLETON(className) \
\
+ (className *)shared ## className \
{ \
    static className *shared ## className = nil; \
    static dispatch_once_t once; \
    dispatch_once(&once, ^ { shared ## className = [[className alloc] init]; }); \
    return shared ## className \
}
#endif

#endif
