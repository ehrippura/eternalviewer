//
//  EWVFileViewItem.h
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2021/12/25.
//  Copyright © 2021 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface EWVFileViewItem : NSCollectionViewItem

@end

NS_ASSUME_NONNULL_END
