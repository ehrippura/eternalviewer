//
//  EWVBrowserController.h
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2013/12/23.
//  Copyright (c) 2013 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class EWVCollectionView;

@import Quartz;

@interface EWVBrowserController : NSWindowController <NSSplitViewDelegate, NSOutlineViewDataSource, NSOutlineViewDelegate, NSMenuDelegate> {
    IBOutlet NSSplitView *          _splitView;
    IBOutlet NSPathControl *        _pathControl;
    IBOutlet NSOutlineView *        _favoriteTableView;
        
    IBOutlet NSView *               _detailView;

    // placeholder
    IBOutlet id                     _leftButtonBar;

    IBOutlet NSMenu *               _itemContextMenu;
    IBOutlet NSMenu *               _collectionMenu;
    
@private
    NSMutableArray *                _containedZipFiles;
}

// zip files for selected item
@property (nonatomic, strong) NSMutableArray *containedZipFiles;

// menu action
- (IBAction)createNewCategory:(id)sender;
- (IBAction)openFolder:(id)sender;

- (IBAction)removeFolder:(id)sender;
- (BOOL)canRowRemoved;

// favorite selection
- (IBAction)selectFavoriteItem:(id)sender;
- (IBAction)zoomSliderValueChanged:(id)sender;

// contextual menu action
- (IBAction)showInFinder:(id)sender;
- (IBAction)showFileInFinder:(id)sender;
- (IBAction)openItem:(id)sender;

@end
