//
//  EWVPluginManager.m
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/5/10.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import "EWVPluginManager.h"
#import "EWVAppDelegate.h"

#import <EWCompress/EWCompress.h>
#import <objc/objc-class.h>

@interface EWVPluginManager () {
    NSMutableDictionary *_classNameDictionary;
    NSMutableDictionary *_pluginIdentifier;
}

- (void)loadPlugins;

@end

@implementation EWVPluginManager

+ (instancetype)sharedManager
{
    static id master = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        master = [[self alloc] init];
    });
    
    return master;
}

- (id)init
{
    self = [super init];
    if (self) {
        _classNameDictionary = [[NSMutableDictionary alloc] init];
        _pluginIdentifier = [[NSMutableDictionary alloc] init];
        
        [self loadPlugins];
    }
    return self;
}

- (BOOL)fileTypeSupported:(NSString *)type
{
    return [_pluginIdentifier objectForKey:[type lowercaseString]] != nil;
}

- (Class)pricipalClassForFileType:(NSString *)type
{
    NSString *identifier = [_pluginIdentifier objectForKey:[type lowercaseString]];
    
    if (identifier) {
        NSString *className = [_classNameDictionary objectForKey:identifier];
        return NSClassFromString(className);
    }
    
    return Nil;
}

// proxy method
- (EWCFile *)zipFileWithFilename:(NSString *)filename
{
    NSString *extension = [filename pathExtension];
    Class pricipal = [self pricipalClassForFileType:extension];
    
    EWCFile *file = [[pricipal alloc] initWithFilename:filename];
    
    return file;
}

- (void)loadPlugins
{
    NSMutableArray *pluginPaths = [[NSMutableArray alloc] init];
    
    // plugins inside application bundle
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *pluginPath = [mainBundle builtInPlugInsPath];
    NSEnumerator *e = [[NSFileManager defaultManager] enumeratorAtPath:pluginPath];

    for (NSString *file in e) {
        NSString *fullpath = [pluginPath stringByAppendingPathComponent:file];
        [pluginPaths addObject:fullpath];
    }

    NSString *userPluginPath = [[applicationFilesLocation() URLByAppendingPathComponent:@"PlugIns"] path];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // plugin in user application support directory
    BOOL isDir = NO;
    if ([fileManager fileExistsAtPath:userPluginPath isDirectory:&isDir] && isDir) {
        NSError *error = nil;
        NSArray *files = [fileManager contentsOfDirectoryAtPath:userPluginPath error:&error];
        
        if (!error) {
            for (NSString *fname in files) {
                NSString *fullname = [userPluginPath stringByAppendingPathComponent:fname];
                NSBundle *bundle = [NSBundle bundleWithPath:fullname];
                if (bundle)
                    [pluginPaths addObject:fullname];
            }
        }
    }
    
    // load plugins to memory
    for (NSString *pluginPath in pluginPaths) {
        NSBundle *bundle = [NSBundle bundleWithPath:pluginPath];
        NSDictionary *info = [bundle infoDictionary];
        NSString *pricipalClass = [info objectForKey:@"NSPrincipalClass"];
        
        if (pricipalClass && ![[_classNameDictionary allValues] containsObject:pricipalClass]) {
            if ([bundle load]) {
                Class fileClass = NSClassFromString(pricipalClass);
                if ([fileClass conformsToProtocol:@protocol(EWCFile)]) {
                    NSString *fileId = [fileClass identifier];
                    [_classNameDictionary setObject:pricipalClass forKey:fileId];
                    NSArray *supportedFileType = [fileClass supportedFileTypes];
                    
                    for (NSString *type in supportedFileType) {
                        [_pluginIdentifier setObject:fileId forKey:type];
                    }
                } else {
                    [bundle unload];
                } // conform protocol
            } // if load
        } // contains object
    }
}

@end
