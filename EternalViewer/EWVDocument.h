//
//  EWVDocument.h
//  EternalViewer
//
//  Created by 林 子頤 on 13/2/19.
//  Copyright (c) 2013 EternalWind. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class EWCFile, EWVCategoryItem, EWVCollectionObject;

@interface EWVDocument : NSDocument <NSPopoverDelegate> {
    EWCFile *_zipFile;
    
    IBOutlet NSPopover *_jumperPopover;
    IBOutlet NSViewController *_popoverContentViewController;
}

// support next/previous item
@property (nonatomic, weak) EWVCategoryItem *categoryItem;

// support main manu enable properity binding
- (BOOL)hasNextImage;
- (BOOL)hasPreviousImage;
- (BOOL)hasNextFile;
- (BOOL)hasPreviousFile;

- (IBAction)toNextItem:(id)sender;
- (IBAction)toPreviousItem:(id)sender;

@end
