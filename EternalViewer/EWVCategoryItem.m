//
//  EWVCategoryItem.m
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/1/27.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import "EWVCategoryItem.h"
#import "EWVCategory.h"

@implementation EWVCategoryItem

@synthesize category = _category;

+ (BOOL)supportsSecureCoding {
    return YES;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _pathURL = [aDecoder decodeObjectOfClass:[NSURL class] forKey:@"path"];
    }
    return self;
}

- (instancetype)initWithPath:(NSString *)path {
    return [self initWithFileURL:[NSURL fileURLWithPath:path]];
}

- (id)initWithFileURL:(NSURL *)url {
    self = [super init];
    if (self) {
        _pathURL = [url copy];
        self.name = [_pathURL lastPathComponent];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.pathURL forKey:@"path"];
}

- (BOOL)isEqual:(id)object {
    EWVCategoryItem *item = object;
    if ([_pathURL isEqualTo:item.pathURL] &&
        [self.name isEqualToString:item.name]) {
        return YES;
    }
    return NO;
}

- (NSString *)description {
    NSString *parent = [super description];
    return [NSString stringWithFormat:@"%@ - Path: %@", parent, _pathURL.absoluteString];
}

// pasteboard read support
+ (NSArray *)readableTypesForPasteboard:(NSPasteboard *)pasteboard {
    return @[(id)kUTTypeFileURL, NSPasteboardTypeString];
}

+ (NSPasteboardReadingOptions)readingOptionsForType:(NSString *)type pasteboard:(NSPasteboard *)pasteboard {
    return NSPasteboardReadingAsString;
}

- (id)initWithPasteboardPropertyList:(id)propertyList ofType:(NSString *)type {

    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *path = nil;

    if (UTTypeConformsTo((__bridge CFStringRef)type, kUTTypeFileURL)) {
        NSURL *url = [[NSURL alloc] initWithPasteboardPropertyList:propertyList ofType:type];
        path = [url path];
    } else {
        path = propertyList;
    }

    BOOL isDirectory = NO;
    if ([fm fileExistsAtPath:path isDirectory:&isDirectory]) {
        if (isDirectory) {
            return [self initWithPath:path];
        }
        return nil;
    }
    return nil;
}

@end

@implementation EWVCategoryItem (PrivateMethods)

- (void)__setCategory:(EWVCategory *)category {
    _category = category;
}

@end
