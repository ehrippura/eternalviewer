//
//  EWVCategoryManager.m
//  EternalViewer
//
//  Created by Tzu-Yi Lin on 2014/04/12.
//  Copyright (c) 2014 EternalWind. All rights reserved.
//

#import "EWVCategoryManager.h"
#import "EWVAppDelegate.h"

static NSURL *fileLocation()
{
    return [applicationFilesLocation() URLByAppendingPathComponent:@"collections.cls"];
}

@implementation EWVCategoryManager

+ (instancetype)sharedManager
{
    static id master = nil;;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        master = [[self alloc] init];
    });
    
    return master;
}

- (id)init
{
    self = [super init];
    if (self) {
        // load file
        NSString *filepath = [fileLocation() path];
        NSData *fileData = [NSData dataWithContentsOfFile:filepath];

        if (fileData) {
            NSSet *decodeClasses = [NSSet setWithObjects:[NSMutableArray class], [EWVCategory class], nil];
            _categories = [NSKeyedUnarchiver unarchivedObjectOfClasses:decodeClasses fromData:fileData error:nil];
        }

        if (!_categories) {
            EWVCategory *newCategory = [EWVCategory new];
            newCategory.name = @"Favorite";
            _categories = [[NSMutableArray alloc] initWithObjects:newCategory, nil];
        } else {
            // scan the folders
            NSFileManager *filemanager = [NSFileManager defaultManager];
            for (EWVCategory *category in _categories) {
                NSMutableArray *remove = [[NSMutableArray alloc] init];
                for (EWVCategoryItem *item in category.items) {
                    if (![filemanager fileExistsAtPath:[item.pathURL path]]) {
                        [remove addObject:item];
                    }
                }
                [category removeItemsInArray:remove];
            }
        }
    }
    return self;
}

- (NSArray *)categories
{
    return [_categories copy];
}

- (EWVCategory *)categoryWithName:(NSString *)name
{
    NSArray *names = [_categories valueForKeyPath:@"@unionOfObjects.name"];
    NSInteger index = [names indexOfObject:name];
    
    if (index == NSNotFound) {
        return nil;
    }
    
    return [_categories objectAtIndex:index];
}

- (EWVCategory *)addCategoryFromName:(NSString *)name
{
    EWVCategory *category = [EWVCategory new];
    if (name) category.name = name;
    
    [_categories addObject:category];
    return category;
}

- (void)moveCategory:(EWVCategory *)category toIndex:(NSInteger)index
{
    [_categories removeObject:category];
    [_categories insertObject:category atIndex:index];
}

- (void)save
{
    NSString *filePath = [fileLocation() path];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:_categories
                                         requiringSecureCoding:YES
                                                         error:nil];

    if (data) {
        [data writeToFile:filePath atomically:YES];
    }
}

@end
